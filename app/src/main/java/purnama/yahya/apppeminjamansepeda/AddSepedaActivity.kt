package purnama.yahya.apppeminjamansepeda

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_addsepeda.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class AddSepedaActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String, Any>>
    lateinit var uri : Uri
    val RC_OK = 100
    var fileName = ""
    var fileType = ""
    var uptype = ""
    var link=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addsepeda)
        btnTambah.setOnClickListener(this)
        btnKembaliAddSepeda.setOnClickListener(this)
        imFotoSepeda.setOnClickListener(this)

        alFile = ArrayList()
        uri = Uri.EMPTY
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("sepeda")
//        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
//            if (firebaseFireStoreException!=null){
//                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
//            }
//        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
            }
        }
    }

    override fun onClick(v: View?) {

        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnTambah ->{
                val progressDialog = ProgressDialog(this)
                progressDialog.isIndeterminate = true
                progressDialog.setMessage("Mengupload Foto...")
                progressDialog.show()
                if (uri != null){
                    fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileName+fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> {task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener{task ->
                            link= task.result.toString()
                            progressDialog.hide()
                            progressDialog.setMessage("Menambahkan data ...")
                            progressDialog.show()
                            val  hm = HashMap<String,Any>()
                            hm.put("kode", edKode.text.toString())
                            hm.put("nama", edNama.text.toString())
                            hm.put("denda",edDenda.text.toString())
                            hm.put("status","Ready")
                            hm.put("foto_name",fileName+fileType)
                            hm.put("foto_url",link)
                            db.document(edKode.text.toString()).set(hm).addOnSuccessListener {
                                progressDialog.hide()
                                Toast.makeText(
                                    this,
                                    "Data Sukses Ditambahkan",
                                    Toast.LENGTH_SHORT
                                ).show()
                                finish()
                            }
                        }
                }

            }
            R.id.imFotoSepeda ->{
                fileType = ".jpg"
                intent.setType("image/*")
                uptype="jpg"
                startActivityForResult(intent,RC_OK)
            }
            R.id.btnKembaliAddSepeda ->{
                finish()
            }
        }
    }

}