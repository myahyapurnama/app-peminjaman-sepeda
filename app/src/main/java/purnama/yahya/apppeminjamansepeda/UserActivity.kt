package purnama.yahya.apppeminjamansepeda

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_user.*

class UserActivity: AppCompatActivity(), View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference

    var username = ""
    var status = ""
    var kd_trx = ""
    var kmbl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        var paket : Bundle? = intent.extras
        username = paket?.getString("username").toString()
//        Log.i("info","$teks")
        txUsernameUser.setText(username)


        db = FirebaseFirestore.getInstance().collection("sepeda")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        btnLogout.setOnClickListener (this)
        btnPinjamSepeda.setOnClickListener (this)
        imageView5.setOnClickListener (this)
        imageView7.setOnClickListener (this)
    }

    override fun onStart() {
        super.onStart()
        cekPinjam(username)
    }

    fun cekPinjam(un: String){
        dbt.whereEqualTo("peminjam",un).get().addOnSuccessListener { result ->
            for(doc in result){
                kd_trx = doc.get("kode_trx").toString()
                kmbl = doc.get("kembali").toString()
                status = doc.get("status").toString()
            }
            if(kd_trx!=""){
                btnPinjamSepeda.visibility = View.GONE
                imageView5.visibility = View.GONE
                imageView7.visibility = View.GONE
                textView9.visibility = View.GONE
                Log.d("status","status $status")

                txNotif1.visibility = View.VISIBLE
                txNotif2.visibility = View.VISIBLE
                txNotif3.visibility = View.VISIBLE
                imNotif1.visibility = View.VISIBLE
                imNotif2.visibility = View.VISIBLE
                txNotif2.text = "JAM\n" + kmbl
                txNotif3.text = "Kode TRX :\n" + kd_trx
            }else{
                imageView5.visibility = View.VISIBLE
                imageView7.visibility = View.VISIBLE
                textView9.visibility = View.VISIBLE
                btnPinjamSepeda.visibility = View.VISIBLE

                txNotif1.visibility = View.GONE
                txNotif2.visibility = View.GONE
                txNotif3.visibility = View.GONE
                imNotif1.visibility = View.GONE
                imNotif2.visibility = View.GONE
            }
        }.addOnFailureListener {
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPinjamSepeda->{
                val intent = Intent(this,ListSepedaActivity::class.java)
                intent.putExtra("username",username)
                startActivity(intent)
            }
            R.id.imageView5->{
                val intent = Intent(this,ListSepedaActivity::class.java)
                intent.putExtra("username",username)
                startActivity(intent)
            }
            R.id.imageView7->{
                val intent = Intent(this,ListSepedaActivity::class.java)
                intent.putExtra("username",username)
                startActivity(intent)
            }
            R.id.btnLogout -> {
                fbAuth.signOut()
                //menutup halaman ketika selesai logoff
                Toast.makeText(this,"Successfully Log out", Toast.LENGTH_SHORT).show()
                finish()
            }
        }
    }
}