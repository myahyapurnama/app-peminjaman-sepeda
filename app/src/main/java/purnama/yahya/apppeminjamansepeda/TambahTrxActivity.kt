package purnama.yahya.apppeminjamansepeda

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_low.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class TambahTrxActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var mediaController: MediaController
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference
    var kode = ""
    var foto = ""
    var username = ""
    var foto_url = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPinjam ->{
                val  hm = HashMap<String,Any>()
                var fileName = "TRX_" + SimpleDateFormat("yyMMddHHmmssSSS").format(Date())
                var rightNow = Calendar.getInstance()
                var currentHour = rightNow.get(Calendar.HOUR_OF_DAY)
                hm.put("kode_trx",fileName)
                hm.put("peminjam",username)
                hm.put("kode_sepeda",kode)
                hm.put("status","Proses")
                hm.put("kembali",currentHour + 5)
                dbt.document(fileName).set(hm).addOnSuccessListener {
                    db.whereEqualTo("peminjam", username).get().addOnSuccessListener { result ->
                        val hm = HashMap<String, Any>()
                        hm.put("kode_trx", fileName)
                        for (doc in result) {
                            db.document(doc.id).update(hm)
                                .addOnSuccessListener {
                                    Log.d("update", "update data $doc.id")
                                }
                        }
                    }
                    Toast.makeText(
                        this,
                        "Sewa Sepeda Berhasil",
                        Toast.LENGTH_SHORT
                    ).show()
                    finish()
                }
            }
            R.id.btnKembali ->{
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_low)
        var paket : Bundle? = intent.extras
        kode = paket?.getString("kode").toString()
        username = paket?.getString("username").toString()
        btnPinjam.setOnClickListener(this)
        btnKembali.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        db = FirebaseFirestore.getInstance().collection("sepeda")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showDetailSepeda(kode)
        }
    }

    fun showDetailSepeda(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                txKodeDetailLow.text = kode
                txNamaDetailLow.text = doc.get("nama").toString()
                txDendaDetailLow.text = doc.get("denda").toString()
                txStatusDetailLow.text = doc.get("status").toString()
                foto = doc.get("foto_name").toString()
                var uri = doc.get("foto_url").toString()
                foto_url = uri
                Picasso.get().load(uri).into(imPoster)
            }
            if (txStatusDetailLow.getText() == "Ready") {
                txStatusDetailLow.setTextColor(Color.GREEN)
                btnPinjam.visibility = View.VISIBLE
            } else {
                txStatusDetailLow.setTextColor(Color.RED)
                btnPinjam.visibility = View.GONE
            }
        }
    }
}