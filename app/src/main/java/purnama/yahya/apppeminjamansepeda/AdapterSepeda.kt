package purnama.yahya.apppeminjamansepeda

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.squareup.picasso.Picasso

class AdapterSepeda (val context : Context,
                     arrayList: ArrayList<HashMap<String, Any>>): BaseAdapter() {
    val KODE_SEPEDA = "kode"
    val NAMA_SEPEDA = "nama"
    val DENDA_SEPEDA = "denda"
    val STATUS_SEPEDA = "status"
    val FOTO_URL_SEPEDA = "foto_url"
    val list = arrayList
    var uri = Uri.EMPTY

    inner class ViewHolder(){
        var txKode : TextView? = null
        var txNama : TextView? = null
        var txDenda : TextView? = null
        var txStatus : TextView? = null
        var imv : ImageView? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var holder = ViewHolder()
        var view: View? = convertView
        if(convertView == null) {
            var inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater
            view = inflater.inflate(R.layout.row_sepeda, null, true)

            holder.txKode = view!!.findViewById(R.id.txKode) as TextView
            holder.txNama = view!!.findViewById(R.id.txNama) as TextView
            holder.txDenda = view!!.findViewById(R.id.txDenda) as TextView
            holder.txStatus = view!!.findViewById(R.id.txStatus) as TextView
            holder.imv = view!!.findViewById(R.id.imv) as ImageView

            view.tag = holder
        }else{
            holder = view!!.tag as ViewHolder
        }
        uri = Uri.parse(list.get(position).get(FOTO_URL_SEPEDA).toString())

        holder.txKode!!.setText(list.get(position).get(KODE_SEPEDA).toString())
        holder.txNama!!.setText(list.get(position).get(NAMA_SEPEDA).toString())
        holder.txDenda!!.setText("Rp " + list.get(position).get(DENDA_SEPEDA).toString())
        holder.txStatus!!.setText(list.get(position).get(STATUS_SEPEDA).toString())

        Picasso.get().load(uri).into(holder.imv)

        if (holder.txStatus!!.getText().toString() == "Ready") {
            holder.txStatus!!.setTextColor(Color.GREEN)
        } else {
            holder.txStatus!!.setTextColor(Color.RED)
        }

        return view!!

    }

    override fun getItem(position: Int): Any {
        return  list.get(position)
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getCount(): Int {
        return list.size
    }

}