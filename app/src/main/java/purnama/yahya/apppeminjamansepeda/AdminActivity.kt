package purnama.yahya.apppeminjamansepeda

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_admin.*
import kotlinx.android.synthetic.main.activity_main.*

class AdminActivity: AppCompatActivity(),View.OnClickListener {
    var fbAuth = FirebaseAuth.getInstance()
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        var paket : Bundle? = intent.extras
        var teks = paket?.getString("username")
        Log.i("info","$teks")

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("trx")

        dataSPD.setOnClickListener {
            val intent = Intent(this, SepedaActivity::class.java)
            startActivity(intent)
        }

        imageView4.setOnClickListener {
            val intent = Intent(this, SepedaActivity::class.java)
            startActivity(intent)
        }

        imageView6.setOnClickListener {
            val intent = Intent(this, SepedaActivity::class.java)
            startActivity(intent)
        }

        dataPinjam.setOnClickListener {
            val intent = Intent(this, PeminjamanActivity::class.java)
            startActivity(intent)
        }

        imageView5.setOnClickListener {
            val intent = Intent(this, PeminjamanActivity::class.java)
            startActivity(intent)
        }

        imageView7.setOnClickListener {
            val intent = Intent(this, PeminjamanActivity::class.java)
            startActivity(intent)
        }

        btnLogout.setOnClickListener {
            fbAuth.signOut()
            //menutup halaman ketika selesai logoff
            Toast.makeText(this, "Successfully Log out", Toast.LENGTH_SHORT).show()
            finish()
        }

    }

    override fun onClick(v: View?) {
        when(v?.id){

        }
    }
}