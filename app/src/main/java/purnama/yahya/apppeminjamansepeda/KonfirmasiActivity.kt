package purnama.yahya.apppeminjamansepeda

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_confirm.*
import kotlinx.android.synthetic.main.activity_editsepeda.*

class KonfirmasiActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var dbt : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: AdapterSepeda
    lateinit var dialog: AlertDialog.Builder
    val KODE_SEPEDA = "kode"
    val NAMA_SEPEDA = "nama"
    val DENDA_SEPEDA = "denda"
    val STATUS_SEPEDA = "status"
    //    val DVD_POSTER_EXT = "poster_type"
    val FOTO_URL_SEPEDA = "foto_url"
    var kd_trx = ""
    var status = ""

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnConfirmTrx ->{
                val hm = HashMap<String, Any>()
                hm.put("status", "Dipinjam")
                dbt.document(kd_trx).update(hm)
                    .addOnSuccessListener {
                        Toast.makeText(this,"Peminjaman dikonfirmasi", Toast.LENGTH_SHORT).show()
                        finish()
                    }
            }
            R.id.btnClearTrx ->{
                dialog.setTitle("Selesaikan?").setMessage("Hapus data transaksi?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog2)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnBatalTrx ->{
                dialog.setTitle("Batalkan?").setMessage("Hapus data transaksi?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnKembaliTrx ->{
                finish()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        kd_trx = paket?.getString("kode_trx").toString()
        btnConfirmTrx.setOnClickListener(this)
        btnBatalTrx.setOnClickListener(this)
        btnKembaliTrx.setOnClickListener(this)
        btnClearTrx.setOnClickListener(this)
        alFile = ArrayList()
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("sepeda")
        dbt = FirebaseFirestore.getInstance().collection("trx")
        dbt.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showTrx(kd_trx)
        }
    }

    fun showTrx(kd:String){
        dbt.whereEqualTo("kode_trx",kd).get().addOnSuccessListener { result ->
            for (doc in result){
                txKdTrans.setText(kd_trx)
                txNamaPeminjam.setText(doc.get("peminjam").toString())
                txStatusTrx.setText(doc.get("status").toString())
                txKembaliTrx.setText(doc.get("kembali").toString())
            }
            if(txStatusTrx.getText() == "Dipinjam"){
                btnConfirmTrx.visibility = View.GONE
                btnClearTrx.visibility = View.VISIBLE
                btnBatalTrx.visibility = View.GONE
            }
        }
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->
        dbt.document(kd_trx).delete()
            .addOnSuccessListener {
                Toast.makeText(this,"Data Sukses Dihapus", Toast.LENGTH_SHORT)
                    .show()
                finish()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this,"Data Gagal Dihapus : ${e.message}", Toast.LENGTH_SHORT)
                    .show() }

    }
    val btnHapusDialog2 = DialogInterface.OnClickListener { dialog, which ->
        val hm = HashMap<String, Any>()
        hm.put("status", "Ready")
        hm.put("peminjam", "")
        hm.put("kode_trx", "")
        hm.put("kembali", "")
        db.document(kd_trx).update(hm)
            .addOnSuccessListener {
            }
        dbt.document(kd_trx).delete()
            .addOnSuccessListener {
                Toast.makeText(this,"Data Sukses Dihapus", Toast.LENGTH_SHORT)
                    .show()
                finish()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this,"Data Gagal Dihapus : ${e.message}", Toast.LENGTH_SHORT)
                    .show() }

    }
}