package purnama.yahya.apppeminjamansepeda

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_sepeda.*

class SepedaActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var alFile : ArrayList<HashMap<String,Any>>
    lateinit var adapter: AdapterSepeda
    lateinit var uri : Uri
    val KODE_SEPEDA = "kode"
    val NAMA_SEPEDA = "nama"
    val DENDA_SEPEDA = "denda"
    val STATUS_SEPEDA = "status"
    val FOTO_URL_SEPEDA = "foto_url"
    val RC_OK = 100
    var kode = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sepeda)

        btnTambahSPD.setOnClickListener(this)
        alFile = ArrayList()
        uri = Uri.EMPTY
        btnKembaliSepeda.setOnClickListener(this)
        lsV.setOnItemClickListener(itemClick)
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        val hm = alFile.get(position)
        kode = hm.get(KODE_SEPEDA).toString()
        Log.i("kode","kode $kode")
        val intent = Intent(this,DetailHighActivity::class.java)
        intent.putExtra(KODE_SEPEDA,kode)
        startActivity(intent)
    }

    override fun onStart() {
        super.onStart()

        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("sepeda")
        db.addSnapshotListener { querySnapshot, firebaseFireStoreException ->
            if (firebaseFireStoreException!=null){
                firebaseFireStoreException.message?.let{ Log.e("Firestore : ",it)}
            }
            showData()
        }
    }

    fun showData(){
        db.get().addOnSuccessListener { result ->
            alFile.clear()
            for (doc in result){
                val hm = HashMap<String,Any>()
                hm.put(KODE_SEPEDA, doc.get(KODE_SEPEDA).toString())
                hm.put(NAMA_SEPEDA, doc.get(NAMA_SEPEDA).toString())
                hm.put(DENDA_SEPEDA, doc.get(DENDA_SEPEDA).toString())
                hm.put(STATUS_SEPEDA, doc.get(STATUS_SEPEDA).toString())
                hm.put(FOTO_URL_SEPEDA, doc.get(FOTO_URL_SEPEDA).toString())
                alFile.add(hm)
            }
            adapter = AdapterSepeda(this,alFile)
            lsV.adapter = adapter
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
            }
        }
    }

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnTambahSPD ->{
                val intent = Intent(this,AddSepedaActivity::class.java)
                startActivity(intent)
            }
            R.id.btnKembaliSepeda ->{
                finish()
            }
        }
    }
}