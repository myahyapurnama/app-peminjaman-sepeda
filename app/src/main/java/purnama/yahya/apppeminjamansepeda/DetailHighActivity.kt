package purnama.yahya.apppeminjamansepeda

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_detail_high.*
import kotlinx.android.synthetic.main.row_sepeda.*

class DetailHighActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var mediaController: MediaController
    lateinit var dialog: AlertDialog.Builder
    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    val KODE_SEPEDA = "kode"
    var kode = ""
    var foto = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_high)
        mediaController = MediaController(this)
        dialog = AlertDialog.Builder(this)
        var paket : Bundle? = intent.extras
        kode = paket?.getString(KODE_SEPEDA).toString()
        btnEditSepeda.setOnClickListener(this)
        btnHapusSepeda.setOnClickListener(this)
        btnKembali.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("sepeda")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            showDetailSepeda(kode)
        }
    }

    fun showDetailSepeda(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                txKodeDetailHigh.text = kode
                txNamaDetailHigh.text = doc.get("nama").toString()
                txStatusDetailHigh.text = doc.get("status").toString()
                txDendaDetailHigh.text = doc.get("denda").toString()
                foto = doc.get("foto_name").toString()
                var uri = doc.get("foto_url").toString()
                Picasso.get().load(uri).into(imPoster)
            }
        }
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnHapusSepeda ->{
                dialog.setTitle("Konfirmasi").setMessage("Yakin akan menghapus data ini?")
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setPositiveButton("Ya", btnHapusDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
            R.id.btnEditSepeda ->{
                val intent = Intent(this,EditSepedaActivity::class.java)
                intent.putExtra(KODE_SEPEDA,kode)
                startActivity(intent)
            }
            R.id.btnKembali ->{
                finish()
            }
        }
    }

    val btnHapusDialog = DialogInterface.OnClickListener { dialog, which ->

        val fileRefP = storage.child(foto)
        fileRefP.delete().addOnSuccessListener {

        }.addOnFailureListener {

        }
        db.whereEqualTo("kode",kode).get()
            .addOnSuccessListener {results ->
                for (doc in results){
                    db.document(doc.id).delete()
                        .addOnSuccessListener {
                            Toast.makeText(this,"Data Sukses Dihapus", Toast.LENGTH_SHORT)
                                .show()
                            finish()
                        }
                        .addOnFailureListener { e ->
                            Toast.makeText(this,"Data Gagal Dihapus : ${e.message}", Toast.LENGTH_SHORT)
                                .show() }
                }
            }.addOnFailureListener { e ->
                Toast.makeText(this,"Tidak bisa menemukan referensi : ${e.message}", Toast.LENGTH_SHORT)
                    .show()
            }
    }
}