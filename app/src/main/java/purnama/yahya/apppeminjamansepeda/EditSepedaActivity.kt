package purnama.yahya.apppeminjamansepeda

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_editsepeda.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class EditSepedaActivity: AppCompatActivity(), View.OnClickListener {

    lateinit var storage : StorageReference
    lateinit var db : CollectionReference
    lateinit var uri : Uri
    var fileName =""
    var fileType =""
    val RC_OK = 100
    var uptype = ""
    var link=""
    var kode=""
    val SEPEDA_KODE="kode"
    var foto=""
    var inisiasi=1

    override fun onClick(v: View?) {
        val intent = Intent()
        intent.action = Intent.ACTION_GET_CONTENT
        when(v?.id){
            R.id.btnEdit -> {
                val progressDialog = ProgressDialog(this)
                progressDialog.isIndeterminate = true
                progressDialog.setMessage("Mengupload Foto...")
                progressDialog.show()
                if (uri !=null) {
                    fileName = SimpleDateFormat("yyyyMMddHHmmssSSS").format(Date())
                    val fileRef = storage.child(fileName + fileType)
                    fileRef.putFile(uri)
                        .continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                            return@Continuation fileRef.downloadUrl
                        })
                        .addOnCompleteListener { taks ->
                            //delete poster lama
                            val fileRefP = storage.child(foto)
                            fileRefP.delete().addOnSuccessListener {
                            }.addOnFailureListener {
                            }
                            link = taks.result.toString()
                            progressDialog.hide()
                            progressDialog.setMessage("Mengupdate data...")
                            progressDialog.show()
                            val hm = HashMap<String, Any>()
                            hm.put("nama", edNama.text.toString())
                            hm.put("denda", edDenda.text.toString())
                            hm.put("status", edStatus.text.toString())
                            hm.put("foto_name", fileName + fileType)
                            hm.put("foto_url", link)
                            db.document(kode).update(hm)
                                .addOnSuccessListener {
                                    progressDialog.hide()
                                    Toast.makeText(
                                        this,
                                        "Data Sukses Diupdate",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    finish()
                                }
                        }
                }
            }
            R.id.imFotoSepeda ->{
                fileType = ".jpg"
                intent.setType("image/*")
                uptype="jpg"
                startActivityForResult(intent,RC_OK)
            }
            R.id.btnKembaliEditSepeda ->{
                finish()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if ((resultCode == Activity.RESULT_OK) && (requestCode == RC_OK)){
            if(data != null) {
                uri = data.data!!
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editsepeda)
        var paket : Bundle? = intent.extras
        kode = paket?.getString(SEPEDA_KODE).toString()
        btnEdit.setOnClickListener(this)
        imFotoSepeda.setOnClickListener(this)
        btnKembaliEditSepeda.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        storage = FirebaseStorage.getInstance().reference
        db = FirebaseFirestore.getInstance().collection("sepeda")
        db.addSnapshotListener { querySnapshot, e ->
            if (e != null) Log.d("fireStore", e.message.toString())
            if(inisiasi==1){showDetailSepeda(kode)}
        }
    }

    fun showDetailSepeda(kd:String){
        db.whereEqualTo("kode",kd).get().addOnSuccessListener { result ->
            for(doc in result){
                edKode.setText(kode)
                edNama.setText(doc.get("nama").toString())
                edDenda.setText(doc.get("denda").toString())
                edStatus.setText(doc.get("status").toString())
                foto = doc.get("foto_name").toString()
//                vwTrailer.seekTo(100)
            }
        }
        inisiasi=0
    }
}